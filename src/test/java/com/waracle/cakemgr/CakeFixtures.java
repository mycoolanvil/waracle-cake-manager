package com.waracle.cakemgr;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CakeFixtures {

    public final static List<CakeEntity> DEFAULT_CAKES = Arrays.asList(
            lemonCheesecake(),
            victoriaSponge(),
            carrotCake(),
            bananaCake(),
            birthdayCake()
    );


    public static CakeEntity lemonCheesecake() {
        return cake("Lemon cheesecake", "A cheesecake made of lemon",
                "https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg");
    }

    public static CakeEntity victoriaSponge() {
        return cake("victoria sponge", "sponge with jam",
                "http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-image-legacy-id--1001468_10.jpg");
    }

    public static CakeEntity carrotCake() {
        return cake("Carrot cake", "Bugs bunnys favourite",
                "http://www.villageinn.com/i/pies/profile/carrotcake_main1.jpg");
    }

    public static CakeEntity bananaCake() {
        return cake("Banana cake", "Donkey kongs favourite",
                "http://ukcdn.ar-cdn.com/recipes/xlarge/ff22df7f-dbcd-4a09-81f7-9c1d8395d936.jpg");
    }

    public static CakeEntity birthdayCake() {
        return cake("Birthday cake", "a yearly treat",
                "http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg");
    }


    public static CakeEntity blackForrest() {
        return cake("black forrest", "from Germany", "blackForrest.jpg");
    }


    public static CakeEntity cake(String title, String desc, String image) {
        CakeEntity cakeEntity = new CakeEntity();

        cakeEntity.setTitle(title);
        cakeEntity.setDesc(desc);
        cakeEntity.setImage(image);

        return cakeEntity;
    }


    public static List<CakeEntity> defaultCakesPlus(CakeEntity... additionalCakes) {
        return Stream.concat(DEFAULT_CAKES.stream(), Stream.of(additionalCakes)).collect(Collectors.toList());
    }
}
