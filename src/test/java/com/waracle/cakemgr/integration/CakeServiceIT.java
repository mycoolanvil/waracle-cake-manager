package com.waracle.cakemgr.integration;

import com.waracle.cakemgr.CakeEntity;
import com.waracle.cakemgr.util.JsonUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static com.waracle.cakemgr.CakeFixtures.*;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CakeServiceIT {

    private static final String CAKES_URI = "http://localhost:8282/cakes";

    @Test
    public void testA_getCakes_shouldReturnDefaultCakeList() throws IOException {
        String actual = httpGetResponseBody(CAKES_URI);

        assertJsonStringContainsCakes(actual, DEFAULT_CAKES);
    }

    @Test
    public void testB_getCakes_multipleRequestsShouldReturnSameResponse() throws IOException {
        String firstResponse = httpGetResponseBody(CAKES_URI);
        String secondResponse = httpGetResponseBody(CAKES_URI);

        assertThat(firstResponse, is(secondResponse));
    }

    @Test
    public void testC_postCake_addsNewCakeToTheEndOfTheList() throws IOException {
        CloseableHttpResponse response = httpPostResponse(CAKES_URI, blackForrest());

        assertThat(response.getStatusLine().getStatusCode(), is(HttpServletResponse.SC_OK));
        assertJsonStringContainsCakes(httpGetResponseBody(CAKES_URI), defaultCakesPlus(blackForrest()));
    }

    @Test
    public void testD_postCake_throwsBadRequestIfCakeAlreadyExists() throws IOException {
        CloseableHttpResponse response = httpPostResponse(CAKES_URI, blackForrest());

        assertThat(response.getStatusLine().getStatusCode(), is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void testE_getCakeWithUserAgent_returnsHtml() throws IOException {
        String actual = browserHttpGetResponseBody(CAKES_URI);

        assertThat(actual, containsString("<html"));
        DEFAULT_CAKES.forEach(cake -> assertThat(actual, containsString(cake.getTitle())));
    }


    private static String httpGetResponseBody(String uri) throws IOException {
        return sendRequestAndReturnResponseBody(new HttpGet(uri));
    }

    private static String browserHttpGetResponseBody(String uri) throws IOException {
        HttpGet get = new HttpGet(uri);
        get.setHeader(HttpHeaders.USER_AGENT, "Mozilla");

        return sendRequestAndReturnResponseBody(get);
    }

    private static String sendRequestAndReturnResponseBody(HttpUriRequest get) throws IOException {
        CloseableHttpResponse response = HttpClientBuilder.create().build().execute(get);

        return EntityUtils.toString(response.getEntity());
    }

    private static CloseableHttpResponse httpPostResponse(String uri, CakeEntity cake) throws IOException {
        HttpPost post = new HttpPost(uri);
        post.setEntity(new StringEntity(JsonUtils.cakeEntityToJsonString(cake)));

        return HttpClientBuilder.create().build().execute(post);
    }

    private static void assertJsonStringContainsCakes(String jsonString, List<CakeEntity> expected) throws IOException {
        List<CakeEntity> actual = JsonUtils.jsonStringToCakeEntities(jsonString);

        assertThat(actual, sameBeanAs(expected));
    }
}
