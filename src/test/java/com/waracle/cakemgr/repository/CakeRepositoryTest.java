package com.waracle.cakemgr.repository;

import com.waracle.cakemgr.CakeEntity;
import com.waracle.cakemgr.exception.CakeAlreadyExistsException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.*;

import java.util.Optional;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static com.waracle.cakemgr.CakeFixtures.cake;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class CakeRepositoryTest {

    private static SessionFactory sessionFactory;
    private Session session;

    private CakeRepository subject;

    @BeforeClass
    public static void start() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Before
    public void setup() {
        session = sessionFactory.openSession();
        clearCakes();

        subject = new CakeRepository(sessionFactory);
    }

    @After
    public void teardown() {
        session.close();
    }

    @AfterClass
    public static void end() {
        sessionFactory.close();
    }


    @Test
    public void shouldStoreCake_andRetrieve() {
        CakeEntity cake = cake("chocolate cake", "cake of chocolate", "chocCake.jpg");

        subject.storeCake(cake);

        assertThat(subject.getCakes(), sameBeanAs(singletonList(cake)));
    }

    @Test
    public void shouldStoreCake_andRetrieveMany() {
        CakeEntity chocolateCake = cake("chocolate cake", "cake of chocolate", "chocCake.jpg");
        CakeEntity lemonCake = cake("lemon cake", "cake of lemon", "lemonCake.jpg");

        subject.storeCake(chocolateCake);
        subject.storeCake(lemonCake);

        assertThat(subject.getCakes(), sameBeanAs(asList(chocolateCake, lemonCake)));
    }

    @Test
    public void shouldNotStoreCake_ifCakeTitleAlreadyExists() {
        CakeEntity chocolateCake = cake("chocolate cake", "cake of chocolate", "chocCake.jpg");
        CakeEntity anotherChocolateCake = cake("chocolate cake", "very chocolatey", "diffChocCake.jpg");

        subject.storeCake(chocolateCake);
         try {
            subject.storeCake(anotherChocolateCake);
            fail("CakeAlreadyExistsException expected");
        } catch (CakeAlreadyExistsException e) {
            assertThat(e.getMessage(), is("Cake with title 'chocolate cake' already exists"));
        }
    }

    @Test
    public void shouldGetCake_givenTitleExists() {
        CakeEntity devilsFoodCake = cake("devils food cake", "one hell of a cake", "devil.gif");
        subject.storeCake(devilsFoodCake);

        Optional<CakeEntity> actual = subject.getCakeByTitle("devils food cake");

        assertThat(actual.isPresent(), is(true));
        assertThat(actual.get(), sameBeanAs(devilsFoodCake));
    }

    @Test
    public void shouldNotGetCake_givenTitleDoesNotExist() {
        Optional<CakeEntity> actual = subject.getCakeByTitle("devils food cake");

        assertThat(actual.isPresent(), is(false));
    }


    private void clearCakes() {
        session.beginTransaction();
        session.createQuery(format("delete from %s", CakeEntity.class.getName())).executeUpdate();
        session.getTransaction().commit();
    }
}
