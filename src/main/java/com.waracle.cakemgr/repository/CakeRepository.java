package com.waracle.cakemgr.repository;

import com.waracle.cakemgr.CakeEntity;
import com.waracle.cakemgr.exception.CakeAlreadyExistsException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.lang.String.format;

public class CakeRepository {

    private final SessionFactory sessionFactory;

    public CakeRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void storeCake(final CakeEntity cake) {
        withinTransaction(session -> {

            Optional<CakeEntity> cakeByTitle = getCakeByTitle(cake.getTitle());

            if(cakeByTitle.isPresent()) {
                throw new CakeAlreadyExistsException(format("Cake with title '%s' already exists", cake.getTitle()));
            }
            else {
                session.persist(cake);
            }

            return true;
        });
    }

    public List<CakeEntity> getCakes() {
        return withinSession(session -> session.createCriteria(CakeEntity.class).list());
    }

    public Optional<CakeEntity> getCakeByTitle(final String cakeTitle) {
        return withinSession(session -> {
            CakeEntity cake = (CakeEntity) session.createCriteria(CakeEntity.class)
                    .add(Restrictions.eq("title", cakeTitle))
                    .uniqueResult();

            return Optional.ofNullable(cake);
        });
    }


    private <T> T withinSession(Function<Session, T> operation) {
        Session session = sessionFactory.openSession();

        try {
            return operation.apply(session);
        } finally {
            session.close();
        }
    }

    private <T> T withinTransaction(Function<Session, T> operation) {
        return withinSession(session -> {
            Transaction transaction = session.beginTransaction();

            T result = operation.apply(session);
            transaction.commit();

            return result;
        });
    }
}
