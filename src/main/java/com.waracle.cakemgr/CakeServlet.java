package com.waracle.cakemgr;

import com.waracle.cakemgr.exception.CakeAlreadyExistsException;
import com.waracle.cakemgr.repository.CakeRepository;
import com.waracle.cakemgr.repository.HibernateUtil;
import com.waracle.cakemgr.util.JsonUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@WebServlet(urlPatterns = {"/", "/cakes"})
public class CakeServlet extends HttpServlet {

    private CakeRepository cakeRepository;

    @Override
    public void init() throws ServletException {
        super.init();

        cakeRepository = new CakeRepository(HibernateUtil.getSessionFactory());

        System.out.println("init started");


        System.out.println("downloading cake json");
        try (InputStream inputStream = new URL("https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json").openStream()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String json = readerToString(reader);

            List<CakeEntity> cakes = JsonUtils.jsonStringToCakeEntities(json);

            removeDuplicateCakesByTitle(cakes).forEach(cakeRepository::storeCake);

        } catch (Exception ex) {
            throw new ServletException(ex);
        }

        System.out.println("init finished");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<CakeEntity> cakes = cakeRepository.getCakes();

        if(isRequestFromBrowser(req)) {
            req.setAttribute("cakes", cakes);
            req.getRequestDispatcher("/cakes.jsp").forward(req, resp);
        }

        String json = JsonUtils.cakeEntitiesToJsonString(cakes);

        resp.getWriter().print(json);
        resp.getWriter().flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String requestBody = readerToString(req.getReader());

        CakeEntity cake = JsonUtils.jsonStringToCakeEntity(requestBody);

        try {
            cakeRepository.storeCake(cake);
        } catch (CakeAlreadyExistsException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private String readerToString(BufferedReader reader) {
        return reader.lines().collect(joining());
    }

    private List<CakeEntity> removeDuplicateCakesByTitle(List<CakeEntity> cakes) {
        //Retaining order as existing test assume insertion order - will later refactor test and this code so order isn't important
        LinkedHashMap<String, CakeEntity> cakesByTitle = cakes.stream().collect(
                LinkedHashMap::new,
                (map, cake) -> {
                    if (!map.containsKey(cake.getTitle())) map.put(cake.getTitle(), cake);
                },
                LinkedHashMap::putAll
        );

        return cakesByTitle.entrySet().stream().map(Map.Entry::getValue).collect(toList());
    }

    /*
     * This should really be going out to a third party trusted web service (e.g. user-agents.org) to check the user agent, however
     * doing a simple check looking for popular browsers (although checking for "mozilla" should work for the majority
     * anyway)
     */
    private boolean isRequestFromBrowser(HttpServletRequest request) {
        Optional<String> optionalUserAgent = Optional.ofNullable(request.getHeader("User-Agent")).map(String::toLowerCase);

        if(optionalUserAgent.isPresent()) {
            String userAgent = optionalUserAgent.get();

            return userAgent.contains("mozilla") || userAgent.contains("firefox") || userAgent.contains("chrome") ||
                    userAgent.contains("opera") || userAgent.contains("msie");
        }

        return false;
    }
}
