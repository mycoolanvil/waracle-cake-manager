package com.waracle.cakemgr.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.waracle.cakemgr.CakeEntity;

import java.io.IOException;
import java.util.List;

public class JsonUtils {

    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public static CakeEntity jsonStringToCakeEntity(String json) throws IOException {
        return OBJECT_MAPPER.readValue(json, CakeEntity.class);
    }

    public static List<CakeEntity> jsonStringToCakeEntities(String json) throws IOException {
        return OBJECT_MAPPER.readValue(json, new TypeReference<List<CakeEntity>>(){});
    }

    public static String cakeEntityToJsonString(CakeEntity cakeEntity) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(cakeEntity);
    }

    public static String cakeEntitiesToJsonString(List<CakeEntity> cakeEntities) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(cakeEntities);
    }
}
