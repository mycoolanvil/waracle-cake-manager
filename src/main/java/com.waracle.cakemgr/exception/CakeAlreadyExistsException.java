package com.waracle.cakemgr.exception;

public class CakeAlreadyExistsException extends RuntimeException {

    public CakeAlreadyExistsException(String message) {
        super(message);
    }
}
