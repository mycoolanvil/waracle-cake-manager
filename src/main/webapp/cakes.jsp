<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html">
        <title>Cake Manager</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            function objectifyForm(formArray) {
                var returnArray = {};
                for (var i = 0; i < formArray.length; i++){
                    returnArray[formArray[i]['name']] = formArray[i]['value'];
                }
                return returnArray;
            }

            $(document).ready(function(){
                $('form').on('submit', function(e){
                    var jsonValue = JSON.stringify(objectifyForm($('form').serializeArray()));
                    e.preventDefault();
                    $.ajax({
                        url: '/cakes',
                        type : "POST",
                        data : jsonValue,
                        success : function(result) {
                            window.location.reload();
                        },
                        error: function(xhr, resp, text) {
                            console.log(xhr, resp, text);
                            alert('Save Failed - please contact administrator');
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <h1>Cake List</h1>

        <table border="2">

            <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            </tr>

        <c:forEach items="${requestScope.cakes}" var="cake">
            <tr>
                <td>${cake.title}</td>
                <td>${cake.desc}</td>
                <td> <img src="${cake.image}" width="80"></td>
            </tr>
        </c:forEach>

        </table>

        <h1>Add New Cake</h1>

            <form id="form" action="" method="post">

                <table>
                    <tr>
                        <td>Title:</td><td><input type="text" name="title"></td>
                    </tr>
                    <tr>
                        <td>Description:</td><td><input type="text" name="desc" ></td>
                    </tr>
                    <tr>
                        <td>Image:</td><td><input type="text" name="image"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            <input id="submit" type="submit" value="Submit" />
                        </td>
                    </tr>
                </table>
            </form>

     </body>
</html>